const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

let root = document.getElementById("root");

function addBooksList(array) {
  const list = array.map(({ author, name, price }) => {
    try {
      if (author && name && price) {
        return `<ul><li>Author: ${author}</li> <li>Name: ${name}</li> <li>Price: ${price}</li></ul>`
      }
      else {
        if (!author) {
          throw new PropertyError("author");
        }
        if (!name) {
          throw new PropertyError("name");
        }
        if (!price) {
          throw new PropertyError("price");
        }
      }
    }

    catch (err) {
      if (err instanceof PropertyError) {
        console.error(err);
      } else {
        throw err;
      }
    }
  }).join("");
  root.insertAdjacentHTML("beforeend", list);
}

class PropertyError {
  constructor(property) {
    this.message = "Отсутствует свойство " + property;
  }
}

addBooksList(books);